%ifndef LIB64_SYSTEMCALLS
%define LIB64_SYSTEMCALLS

; ############# System call macros  ##############

; http://www.opensource.apple.com/source/xnu/xnu-1504.3.12/bsd/kern/syscalls.master

; 1	AUE_EXIT	ALL	{ void exit(int rval); } 
%macro	syscall_exit 1
	move	rax, 0x2000001
	move	rdi, %1
	syscall
%endmacro


; 4	AUE_NULL	ALL	{ user_ssize_t write(int fd, user_addr_t cbuf, user_size_t nbyte); } 
%macro	syscall_write 3
	move	rax, 0x2000004
	move	rdi, %1
	move	rsi, %2
	move	rdx, %3
	syscall
%endmacro

%endif
