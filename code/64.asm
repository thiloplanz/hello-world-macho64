; ./nasm -f macho64 64.asm && ld -macosx_version_min 10.7.0 -o 64 64.o && ./64

%include "lib64/strings.asm"

global start

%define STDOUT	1

section .text

%include "lib64.asm"


start:
	mov		rax,	[rsp]	; argument count
	mov		rsi,	rsp		; next argument on the stack
	; skip the first argument (program name)
	add		rsi, 8
	dec		rax
	
	push rsi
	push rax
	cstring_write STDOUT, msg1
	digits_write	STDOUT, [rsp] 
	cstring_write STDOUT, msg2
	pop rax
	pop rsi
	
	.loop:
		cmp		rax, 	0
		je	.done
		add	rsi, 8
		push	rsi
		push 	rax
		cstring_writeln	STDOUT, [rsi]
		pop 	rax
		pop		rsi
		dec	rax
		jmp	.loop
		
	.done:
		syscall_exit	0



section .rodata


msg1:	db `You gave me \0`
msg2:	db ` arguments:\n\0`


