; ############## basic helper macros #########

%ifndef LIB64_BASE
%define LIB64_BASE

; don't assign a register to itself
%macro move 2
	%ifnidn %1, %2
	mov	%1, %2
	%endif
%endmacro

%endif
