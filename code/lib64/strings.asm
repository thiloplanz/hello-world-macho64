; String handling

%ifndef LIB64_STRINGS
%define LIB64_STRINGS

%include "lib64/base.asm"
%include "lib64/systemcalls.asm"

; call syscall_write for a cstring
%macro cstring_write 2
	; loop over the string in rsi until we find the terminating 0-byte, count up in rdx to get the length
	move 	rsi, %2
	move	rdx, 0
	%%loop:
		cmp	byte[rsi+rdx], 0
		je	%%write
		inc	rdx
		jmp	%%loop
	%%write:
		syscall_write	%1, rsi, rdx	
%endmacro
%macro cstring_writeln 2
	cstring_write	%1, %2
	byte_write	%1, `\n`
%endmacro

; call syscall_write for a single byte
%macro byte_write	2
	; push the byte onto the stack
	sub	rsp, 8
	mov	byte[rsp],  %2
	syscall_write	%1, rsp, 1
	; pop the stack back
	add	rsp, 8
%endmacro
%macro byte_writeln	2
	; push two bytes onto the stack
	sub	rsp, 8
	mov	byte[rsp],  %2
	mov	byte[rsp+1], `\n`
	syscall_write	%1, rsp, 2
	; pop the stack back
	add	rsp, 8
%endmacro


%macro	digits_write 2
	move	rsi, %1
	move	rax, %2		; can only divide rax
	mov		r9, 10		; need the divisor in a register, too
	call	_lib64_strings_digits_write
%endmacro


%endif
