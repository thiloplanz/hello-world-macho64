; String handling

%include "lib64/strings.asm"

_lib64_strings_digits_write:
	cmp		rax, 0
	jl		.negative
	cmp		rax, r9
	jl		.write
	; more than one digit ?
	; push the tail on the stack and recurse for the head
	div		r9		; rax /= 10 ;  rdx = rax % 10
	push	rdx
	push	rsi	; need to save rsi
	digits_write	rsi, rax
	pop		rsi
	; when returning, pop the tail back into rax
	pop 	rax
	.write:
		add		rax, '0'     ; convert digit to ascii
		byte_write	rsi, al
		ret
	.negative:
		neg		rax
		push		rax
		push		rsi
		byte_write	rsi, '-'
		pop		rsi
		pop 		rax
		jmp		_lib64_strings_digits_write


